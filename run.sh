#!/bin/sh
BASE=`dirname $0`
PROJECT=compte-agglo-montpellier
CTL=$BASE/${PROJECT}
VENV=$BASE/${PROJECT}-venv

if [ ! -n "$VIRTUAL_ENV" ]; then
	if [ ! -d $VENV ]; then
	$BASE/start.sh norun
	fi
	. $VENV/bin/activate
fi
$CTL "${@:-runserver}"

