#!/bin/bash -e
pip install --upgrade setuptools
pip install --upgrade pip
pip install --upgrade pylint
pip install --upgrade pyOpenSSL ndg-httpsclient requests pyasn1
sed -i 's/^MAX = 64/MAX = 200/' $VIRTUAL_ENV/lib/python*/site-packages/ndg/httpsclient/subj_alt_name.py

pip install --upgrade -r requirements.txt

rm -f compte-agglo-montpellier.db # remove previous db
./compte-agglo-montpellier syncdb --migrate --noinput
./compte-agglo-montpellier validate
(pylint -f parseable --rcfile /var/lib/jenkins/pylint.django.rc compte_agglo_montpellier/ | tee pylint.out) || /bin/true
