# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        db.delete_column('djangocms_text_ckeditor_text', 'wrapper')

    def backwards(self, orm):
        pass

    models = {
        
    }

    complete_apps = ['compte_agglo_montpellier']
