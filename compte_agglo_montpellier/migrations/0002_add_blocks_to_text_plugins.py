# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models

from djangocms_text_ckeditor.models import Text

class Migration(DataMigration):

    def forwards(self, orm):
        "Write your forwards methods here."
        # Note: Don't use "from appname.models import ModelName". 
        # Use orm.ModelName to refer to models in this application,
        # and orm['appname.ModelName'] for models in other applications.
        for plugin in Text.objects.filter(plugin_type="TextPlugin"):
            plugin.body = '<div class="block">%s</div>' % plugin.body
            plugin.save()

    def backwards(self, orm):
        "Write your backwards methods here."

    models = {
        
    }

    complete_apps = ['compte_agglo_montpellier']
    symmetrical = True
