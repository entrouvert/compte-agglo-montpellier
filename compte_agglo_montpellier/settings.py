import json
import os
import logging.handlers
from django.conf.global_settings import PASSWORD_HASHERS
from django.core.exceptions import ImproperlyConfigured

gettext_noop = lambda s: s
# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'compte_agglo_montpellier.wsgi.application'
DEBUG = 'DEBUG' in os.environ
DEBUG_PROPAGATE_EXCEPTIONS = 'DEBUG_PROPAGATE_EXCEPTIONS' in os.environ
USE_DEBUG_TOOLBAR = 'USE_DEBUG_TOOLBAR' in os.environ
TEMPLATE_DEBUG = DEBUG

def to_boolean(name, default=True):
    try:
        value = os.environ[name]
    except KeyError:
        return default
    try:
        i = int(value)
        return bool(i)
    except ValueError:
        if value.lower() in ('true', 't'):
            return True
        if value.lower() in ('false', 'f'):
            return False
    return default

def to_int(name, default):
    try:
        value = os.environ[name]
        return int(value)
    except KeyError:
        return default
    except ValueError:
        raise ImproperlyConfigured('environ variable %s must be an integer' % name)

def __dict_from_vars(prefix):
    d = {}
    for key in os.environ:
        if key.startswith(prefix):
            d[key[len(prefix):]] = os.environ[key]
    return d


BASE_DIR = os.path.dirname(__file__)
PROJECT_PATH = os.path.join(BASE_DIR, '..')
PROJECT_NAME = 'compte-agglo-montpellier'

ADMINS = ()
if 'ADMINS' in os.environ:
    ADMINS = filter(None, os.environ.get('ADMINS').split(':'))
    ADMINS = [ admin.split(';') for admin in ADMINS ]
    for admin in ADMINS:
        assert len(admin) == 2, 'ADMINS setting must be a colon separated list of name and emails separated by a semi-colon'
        assert '@' in admin[1], 'ADMINS setting pairs second value must be emails'

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': os.environ.get('DATABASE_ENGINE', 'django.db.backends.sqlite3'),
        'NAME': os.environ.get('DATABASE_NAME', os.path.join(PROJECT_PATH, PROJECT_NAME + '.db')),
    }
}

# Hey Entr'ouvert is in France !!
TIME_ZONE = 'Europe/Paris'
LANGUAGE_CODE = 'fr'
SITE_ID = 1
USE_I18N = True

LANGUAGES = (
    ('fr', gettext_noop('French')),
)
USE_L10N = True

# Static files

STATIC_ROOT = os.environ.get('STATIC_ROOT', '/var/lib/%s/static' % PROJECT_NAME)
STATIC_URL = os.environ.get('STATIC_URL', '/static/')
MEDIA_ROOT = os.environ.get('MEDIA_ROOT', '/var/lib/%s/media' % PROJECT_NAME)
MEDIA_URL = os.environ.get('MEDIA_URL', '/media/')

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.request',
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.static',
    'cms.context_processors.cms_settings',
    'sekizai.context_processors.sekizai',
    'portail_citoyen.context_processors.portail_citoyen',
    'authentic2.context_processors.a2_processor',
)

MIDDLEWARE_CLASSES = (
    'entrouvert.djommon.middleware.VersionMiddleware',
    'entrouvert.djommon.middleware.UserInTracebackMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.http.ConditionalGetMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.transaction.TransactionMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'authentic2.idp.middleware.DebugMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
)

ROOT_URLCONF = 'compte_agglo_montpellier.urls'

VAR_DIR = os.path.join('/var/lib/', PROJECT_NAME)

LOCALE_PATHS = (
        os.path.join(VAR_DIR, 'locale'),
        os.path.join(BASE_DIR, 'locale'),
)

TEMPLATE_DIRS = (
        os.path.join(VAR_DIR, 'templates'),
        os.path.join(BASE_DIR, 'templates'),
)

STATICFILES_DIRS = (
        os.path.join(VAR_DIR, 'extra-static'),
        os.path.join(BASE_DIR, 'static'),
)

if os.environ.get('TEMPLATE_DIRS'):
    TEMPLATE_DIRS = tuple(os.environ['TEMPLATE_DIRS'].split(':')) + TEMPLATE_DIRS

if os.environ.get('STATICFILES_DIRS'):
    STATICFILES_DIRS = tuple(os.environ['STATICFILES_DIRS'].split(':')) + STATICFILES_DIRS

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'admin_tools',
    'admin_tools.theming',
    'admin_tools.menu',
    'admin_tools.dashboard',
    'django.contrib.admin',
    'captcha',
    'django_select2',
    'registration',
    'south',
    'mptt',
    'sekizai',
    'cms',
    'djangocms_text_ckeditor',
    'djangocms_video',
    'cms_ajax_text_plugin',
    'menus',
    'authentic2',
    'authentic2.nonce',
    'authentic2.saml',
    'authentic2.idp',
    'authentic2.idp.saml',
    'authentic2.idp.idp_openid',
    'authentic2.auth2_auth',
    'authentic2.attribute_aggregator',
    'authentic2.disco_service',
    'portail_citoyen_announces',
    'portail_citoyen',
    'wcsinst.wcsinst',
    'portail_citoyen_announces',
    'portail_citoyen.apps.feed_plugin',
    'portail_citoyen.apps.data_source_plugin',
    'portail_citoyen.apps.federation_plugin',
    'portail_citoyen.apps.auquotidien_plugin',
    'portail_citoyen.apps.passerelle_register_plugin',
    'compte_agglo_montpellier',
)

MESSAGE_STORAGE = 'django.contrib.messages.storage.session.SessionStorage'


############################
# Registration
############################
ACCOUNT_ACTIVATION_DAYS = to_int('ACCOUNT_ACTIVATION_DAYS', 3)
PASSWORD_RESET_TIMEOUT_DAYS = to_int('PASSWORD_RESET_TIMEOUT_DAYS', 3)
REGISTRATION_OPEN = to_boolean('REGISTRATION_OPEN')
A2_REGISTRATION_CAN_DELETE_ACCOUNT = to_boolean('A2_REGISTRATION_CAN_DELETE_ACCOUNT')
A2_CAN_RESET_PASSWORD = to_boolean('A2_CAN_RESET_PASSWORD')
A2_REGISTRATION_EMAIL_IS_UNIQUE = to_boolean('A2_REGISTRATION_EMAIL_IS_UNIQUE')
A2_REGISTRATION_FORM_CLASS = 'compte_agglo_montpellier.registration_forms.RegistrationForm'
A2_REGISTRATION_SET_PASSWORD_FORM_CLASS = 'compte_agglo_montpellier.registration_forms.SetPasswordForm'
A2_REGISTRATION_CHANGE_PASSWORD_FORM_CLASS = 'compte_agglo_montpellier.registration_forms.PasswordChangeForm'
CAPTCHA_CHALLENGE_FUNCT = 'captcha.helpers.math_challenge'
CAPTCHA_NOISE_FUNCTIONS = ()
CAPTCHA_LETTER_ROTATION = None

# authentication
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)
AUTH_USER_MODEL = 'portail_citoyen.Citoyen'
PASSWORD_HASHERS += (
    'authentic2.hashers.Drupal7PasswordHasher',
)

# sessions
SESSION_EXPIRE_AT_BROWSER_CLOSE =  'SESSION_EXPIRE_AT_BROWSER_CLOSE' in os.environ
SESSION_COOKIE_AGE = int(os.environ.get('SESSION_COOKIE_AGE', 36000)) # one day of work
SESSION_COOKIE_NAME = os.environ.get('SESSION_COOKIE_NAME', 'sessionid')
SESSION_COOKIE_PATH = os.environ.get('SESSION_COOKIE_PATH', '/')
SESSION_COOKIE_SECURE = 'SESSION_COOKIE_SECURE' in os.environ

# email settings
EMAIL_HOST = os.environ.get('EMAIL_HOST', 'localhost')
EMAIL_HOST_USER = os.environ.get('EMAIL_HOST_USER', '')
EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_HOST_PASSWORD', '')
EMAIL_PORT = int(os.environ.get('EMAIL_PORT', 25))
EMAIL_SUBJECT_PREFIX = os.environ.get('EMAIL_SUBJECT_PREFIX', '[Compte Agglo Montpellier]')
EMAIL_USE_TLS = 'EMAIL_USE_TLS' in os.environ
SERVER_EMAIL = os.environ.get('SERVER_EMAIL', 'root@localhost')
DEFAULT_FROM_EMAIL = os.environ.get('DEFAULT_FROM_EMAIL', 'e-services@montpellier-agglo.com')

# web & network settings
if 'ALLOWED_HOSTS' in os.environ:
    ALLOWED_HOSTS = os.environ['ALLOWED_HOSTS'].split(':')
USE_X_FORWARDED_HOST = 'USE_X_FORWARDED_HOST' in os.environ
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTOCOL', 'https')
LOGIN_REDIRECT_URL = os.environ.get('LOGIN_REDIRECT_URL', '/')
LOGIN_URL = os.environ.get('LOGIN_URL', '/login')
LOGOUT_URL = os.environ.get('LOGOUT_URL', '/accounts/logout')

if 'INTERNAL_IPS' in os.environ:
    INTERNAL_IPS = os.environ['INTERNAL_IPS'].split(':')
else:
    INTERNAL_IPS = ('127.0.0.1',)

# cache
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    },
}
if 'CACHE_BACKEND' in os.environ:
    CACHES['default'] = json.loads(os.environ['CACHE_BACKEND'])

# misc
SECRET_KEY = os.environ.get('SECRET_KEY', '0!=(1kc6kri-ui+tmj@mr+*0bvj!(p*r0duu2n=)7@!p=pvf9n')
DEBUG_TOOLBAR_CONFIG = {'INTERCEPT_REDIRECTS': False}
PORTAIL_CITOYEN_TEMPLATE_VARS = {
    'SITE_AGGLO_MONTPELLIER': 'http://www.montpellier-agglo.com/',
    'ESERVICES': 'https://eservices.montpellier-agglo.com/',
}
PORTAIL_CITOYEN_TEMPLATE_VARS.update(__dict_from_vars('PORTAIL_CITOYEN_TEMPLATE_VARS_'))

# Authentic2 settings

DISCO_SERVICE = 'DISCO_SERVICE' in os.environ
DISCO_USE_OF_METADATA = 'DISCO_USE_OF_METADATA' in os.environ

DISCO_SERVICE_NAME = os.environ.get('DISCO_SERVICE_NAME', "http://www.identity-hub.com/disco_service/disco")
DISCO_RETURN_ID_PARAM = "entityID"
SHOW_DISCO_IN_MD = 'SHOW_DISCO_IN_MD' in os.environ
USE_DISCO_SERVICE = 'USE_DISCO_SERVICE' in os.environ

###########################
# Authentication settings
###########################

# Only RSA private keys are currently supported
AUTH_FRONTENDS = ( 'authentic2.auth_frontends.LoginPasswordBackend',)
SSLAUTH_CREATE_USER = 'SSLAUTH_CREATE_USER' in os.environ
AUTHENTICATION_EVENT_EXPIRATION = int(os.environ.get('AUTHENTICATION_EVENT_EXPIRATION', 3600*24*7))

#############################
# Identity Provider settings
#############################

# List of IdP backends, mainly used to show available services in the homepage
# of user, and to handle SLO for each protocols
IDP_BACKENDS = (
    'authentic2.idp.saml.backend.SamlBackend',
    'authentic2.idp.idp_openid.backend.OpenIDBackend',
)
TEMPLATE_CONTEXT_PROCESSORS += ('authentic2.idp.idp_openid.context_processors.openid_meta',)
RESTRICT_OPENID_RP = ["http://data.montpellier-agglo.com/", ]
IDP_OPENID = True
IDP_SAML2 = True
A2_IDP_SAML2_ENABLE = True

# You MUST changes these keys, they are just for testing !
LOCAL_METADATA_CACHE_TIMEOUT = int(os.environ.get('LOCAL_METADATA_CACHE_TIMEOUT', 600))
SAML_SIGNATURE_PUBLIC_KEY = os.environ.get('SAML_SIGNATURE_PUBLIC_KEY', '''-----BEGIN CERTIFICATE-----
MIIDIzCCAgugAwIBAgIJANUBoick1pDpMA0GCSqGSIb3DQEBBQUAMBUxEzARBgNV
BAoTCkVudHJvdXZlcnQwHhcNMTAxMjE0MTUzMzAyWhcNMTEwMTEzMTUzMzAyWjAV
MRMwEQYDVQQKEwpFbnRyb3V2ZXJ0MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIB
CgKCAQEAvxFkfPdndlGgQPDZgFGXbrNAc/79PULZBuNdWFHDD9P5hNhZn9Kqm4Cp
06Pe/A6u+g5wLnYvbZQcFCgfQAEzziJtb3J55OOlB7iMEI/T2AX2WzrUH8QT8NGh
ABONKU2Gg4XiyeXNhH5R7zdHlUwcWq3ZwNbtbY0TVc+n665EbrfV/59xihSqsoFr
kmBLH0CoepUXtAzA7WDYn8AzusIuMx3n8844pJwgxhTB7Gjuboptlz9Hri8JRdXi
VT9OS9Wt69ubcNoM6zuKASmtm48UuGnhj8v6XwvbjKZrL9kA+xf8ziazZfvvw/VG
Tm+IVFYB7d1x457jY5zjjXJvNysoowIDAQABo3YwdDAdBgNVHQ4EFgQUeF8ePnu0
fcAK50iBQDgAhHkOu8kwRQYDVR0jBD4wPIAUeF8ePnu0fcAK50iBQDgAhHkOu8mh
GaQXMBUxEzARBgNVBAoTCkVudHJvdXZlcnSCCQDVAaInJNaQ6TAMBgNVHRMEBTAD
AQH/MA0GCSqGSIb3DQEBBQUAA4IBAQAy8l3GhUtpPHx0FxzbRHVaaUSgMwYKGPhE
IdGhqekKUJIx8et4xpEMFBl5XQjBNq/mp5vO3SPb2h2PVSks7xWnG3cvEkqJSOeo
fEEhkqnM45b2MH1S5uxp4i8UilPG6kmQiXU2rEUBdRk9xnRWos7epVivTSIv1Ncp
lG6l41SXp6YgIb2ToT+rOKdIGIQuGDlzeR88fDxWEU0vEujZv/v1PE1YOV0xKjTT
JumlBc6IViKhJeo1wiBBrVRIIkKKevHKQzteK8pWm9CYWculxT26TZ4VWzGbo06j
o2zbumirrLLqnt1gmBDvDvlOwC/zAAyL4chbz66eQHTiIYZZvYgy
-----END CERTIFICATE-----''')

SAML_SIGNATURE_PRIVATE_KEY = os.environ.get('SAML_SIGNATURE_PRIVATE_KEY', '''-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAvxFkfPdndlGgQPDZgFGXbrNAc/79PULZBuNdWFHDD9P5hNhZ
n9Kqm4Cp06Pe/A6u+g5wLnYvbZQcFCgfQAEzziJtb3J55OOlB7iMEI/T2AX2WzrU
H8QT8NGhABONKU2Gg4XiyeXNhH5R7zdHlUwcWq3ZwNbtbY0TVc+n665EbrfV/59x
ihSqsoFrkmBLH0CoepUXtAzA7WDYn8AzusIuMx3n8844pJwgxhTB7Gjuboptlz9H
ri8JRdXiVT9OS9Wt69ubcNoM6zuKASmtm48UuGnhj8v6XwvbjKZrL9kA+xf8ziaz
Zfvvw/VGTm+IVFYB7d1x457jY5zjjXJvNysoowIDAQABAoIBAQCj8t2iKXya10HG
V6Saaeih8aftoLBV38VwFqqjPU0+iKqDpk2JSXBhjI6s7uFIsaTNJpR2Ga1qvns1
hJQEDMQSLhJvXfBgSkHylRWCpJentr4E3D7mnw5pRsd61Ev9U+uHcdv/WHP4K5hM
xsdiwXNXD/RYd1Q1+6bKrCuvnNJVmWe0/RV+r3T8Ni5xdMVFbRWt/VEoE620XX6c
a9TQPiA5i/LRVyie+js7Yv+hVjGOlArtuLs6ECQsivfPrqKLOBRWcofKdcf+4N2e
3cieUqwzC15C31vcMliD9Hax9c1iuTt9Q3Xzo20fOSazAnQ5YBEExyTtrFBwbfQu
ku6hp81pAoGBAN6bc6iJtk5ipYpsaY4ZlbqdjjG9KEXB6G1MExPU7SHXOhOF0cDH
/pgMsv9hF2my863MowsOj3OryVhdQhwA6RrV263LRh+JU8NyHV71BwAIfI0BuVfj
6r24KudwtUcvMr9pJIrJyMAMaw5ZyNoX7YqFpS6fcisSJYdSBSoxzrzVAoGBANu6
xVeMqGavA/EHSOQP3ipDZ3mnWbkDUDxpNhgJG8Q6lZiwKwLoSceJ8z0PNY3VetGA
RbqtqBGfR2mcxHyzeqVBpLnXZC4vs/Vy7lrzTiHDRZk2SG5EkHMSKFA53jN6S/nJ
JWpYZC8lG8w4OHaUfDHFWbptxdGYCgY4//sjeiuXAoGBANuhurJ99R5PnA8AOgEW
4zD1hLc0b4ir8fvshCIcAj9SUB20+afgayRv2ye3Dted1WkUL4WYPxccVhLWKITi
rRtqB03o8m3pG3kJnUr0LIzu0px5J/o8iH3ZOJOTE3iBa+uI/KHmxygc2H+XPGFa
HGeAxuJCNO2kAN0Losbnz5dlAoGAVsCn94gGWPxSjxA0PC7zpTYVnZdwOjbPr/pO
LDE0cEY9GBq98JjrwEd77KibmVMm+Z4uaaT0jXiYhl8pyJ5IFwUS13juCbo1z/u/
ldMoDvZ8/R/MexTA/1204u/mBecMJiO/jPw3GdIJ5phv2omHe1MSuSNsDfN8Sbap
gmsgaiMCgYB/nrTk89Fp7050VKCNnIt1mHAcO9cBwDV8qrJ5O3rIVmrg1T6vn0aY
wRiVcNacaP+BivkrMjr4BlsUM6yH4MOBsNhLURiiCL+tLJV7U0DWlCse/doWij4U
TKX6tp6oI+7MIJE6ySZ0cBqOiydAkBePZhu57j6ToBkTa0dbHjn1WA==
-----END RSA PRIVATE KEY-----''')

# Whether to autoload SAML 2.0 identity providers and services metadata
# Only https URLS are accepted.
# Can be none, sp, idp or both
SAML_METADATA_AUTOLOAD = os.environ.get('SAML_METADATA_AUTOLOAD', 'none')

PUSH_PROFILE_UPDATES = 'PUSH_PROFILE_UPDATES' in os.environ

##################################
# Raven
##################################
RAVEN_CONFIG_DSN = os.environ.get('RAVEN_CONFIG_DSN', '')
PORTAIL_CITOYEN_TEMPLATE_VARS['SENTRY_JS'] = RAVEN_CONFIG_DSN

# Logging settings

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'filters': {
        'cleaning': {
            '()':  'authentic2.utils.CleanLogMessage',
        },
    },
    'formatters': {
        'syslog': {
            'format': 'portail-citoyen(pid=%(process)d) %(levelname)s %(name)s: %(message)s',
        },
        'syslog_debug': {
            'format': 'portail-citoyen(pid=%(process)d) %(levelname)s %(asctime)s t_%(thread)s %(name)s: %(message)s',
        },
    },
    'handlers': {
        'syslog': {
            'level': 'DEBUG',
            'class': 'entrouvert.logging.handlers.SysLogHandler',
            'formatter': 'syslog_debug' if DEBUG else 'syslog',
            'facility': logging.handlers.SysLogHandler.LOG_LOCAL0,
            'address': '/dev/log',
            'max_length': 999,
            'filters': ['cleaning'],
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'filters': ['cleaning'],
            'include_html': True,
        },
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'syslog_debug',
            'level': 'DEBUG',
        },
    },
    'loggers': {
        'authentic2': {
            'handlers': ['mail_admins','syslog'],
            'level': 'DEBUG' if DEBUG else 'INFO',
            'propagate': False,
        },
        'requests': {
            'handlers': ['mail_admins','syslog'],
            'level': 'ERROR',
            'propagate': False,
        },
        'portail_citoyen': {
            'handlers': ['mail_admins','syslog'],
            'level': 'DEBUG' if DEBUG else 'INFO',
            'propagate': False,
        },
        'django': {
            'handlers': ['mail_admins','syslog'],
            'level': 'DEBUG' if DEBUG else 'INFO',
            'propagate': False,
        },
        'django.db': {
            'handlers': ['mail_admins','syslog'],
            'level': 'INFO',
            'propagate': False,
        },
        'south': {
            'handlers': ['mail_admins','syslog'],
            'level': 'INFO',
            'propagate': False,
        },
        'django_select2': {
            'handlers': ['mail_admins','syslog'],
            'level': 'INFO',
            'propagate': False,
        },
        '': {
            'handlers': ['mail_admins','syslog'],
            'level': 'DEBUG' if DEBUG else 'INFO',
            'propagate': True,
        },
    }
}
if DEBUG:
    for key, logger in LOGGING['loggers'].iteritems():
        logger['handlers'].append('console')
SOUTH_TESTS_MIGRATE = False

# Admin tools
ADMIN_TOOLS_INDEX_DASHBOARD = 'compte_agglo_montpellier.dashboard.CustomIndexDashboard'
ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'compte_agglo_montpellier.dashboard.CustomAppIndexDashboard'
ADMIN_TOOLS_MENU = 'compte_agglo_montpellier.menu.CustomMenu'
ADMIN_TOOLS_THEMING_CSS = 'portail_citoyen/css/admin.css'

# cms settings
CMS_TEMPLATES = (
    ('base_two_columns.html', 'Canevas sur deux colonnes'),
    ('base_one_column.html', 'Canevas sur une colonne'),
    ('base_help.html', 'Canevas de l\'aide'),
    ('base_empty.html', 'Canevas vide'),
)
CMS_REDIRECTS = True
CMS_TEXT_WRAPPERS = (
    ('block', {
        'render_template': 'block.html',
        'extra_context':  {},
    }),
)

PORTAIL_CITOYEN_FAVICON_URL = 'http://www.montpellier-agglo.com/sites/all/themes/zen_agglo/favicon.png'
PORTAIL_CITOYEN_WCSINST_DEFAULT_VARIABLES = {
        'commune_url': '',
        'commune_legal_url': '',
        'domain_key': '',
}

ANNOUNCES_TRANSPORTS = (
        'portail_citoyen_announces.transports.EmailTransport',
        ('portail_citoyen_announces.transports.SMSTransport', {
         'url': os.environ.get('PORTAIL_CITOYEN_SMS_URL', ''),
         'from_mobile': os.environ.get('PORTAIL_CITOYEN_SMS_FROM', ''),
         'login': os.environ.get('PORTAIL_CITOYEN_SMS_LOGIN', ''),
         'password': os.environ.get('PORTAIL_CITOYEN_SMS_PASSWORD', ''),
         }),
        )

##################################
# Portail Admin
##################################
WCSINSTD_URL = os.environ.get('WCSINSTD_URL', 'http://localhost:8092/')
WCSINST_URL_TEMPLATE = os.environ.get('WCSINST_URL_TEMPLATE',
    'https://%(domain)s.eservices.montpellier-agglo.com')

##################################
# MSP
##################################
if 'MSP' in os.environ:
    INSTALLED_APPS += ('portail_citoyen.apps.msp',)
    AUTHENTICATION_BACKENDS += ('portail_citoyen.apps.msp.backends.MspBackend',)
    if os.environ['MSP'] == 'prod':
        MSP_AUTHORIZE_URL = 'https://mon.service-public.fr/apis/app/oauth/authorize'
        MSP_TOKEN_URL = 'https://mon.service-public.fr/apis/app/oauth/token'
        MSP_API_URL = 'https://mon.service-public.fr/apis/'
        MSP_MORE_URL = 'https://mon.service-public.fr/portail/app/cms/public/a_propos_du_site'
    if os.environ['MSP'] == 'prep':
        MSP_AUTHORIZE_URL = 'https://mon.service-public-prep.fr/apis/app/oauth/authorize'
        MSP_TOKEN_URL = 'https://mon.service-public-prep.fr:2443/apis/app/oauth/token'
        MSP_API_URL = 'https://mon.service-public-prep.fr:2443/apis/'
        MSP_MORE_URL = 'https://mon.service-public-prep.fr/portail/app/cms/public/a_propos_du_site'
    MSP_CLIENT_CERTIFICATE = os.environ['MSP_CLIENT_CERTIFICATE'].split(':')
    MSP_CLIENT_ID = os.environ['MSP_CLIENT_ID']
    MSP_CLIENT_SECRET = os.environ['MSP_CLIENT_SECRET']
    MSP_CLIENT_CREDENTIALS = []
    for key, value in os.environ.iteritems():
        if key.startswith('MSP_CLIENT_CREDENTIALS_'):
            client_credentials = value.split(':')
            if len(client_credentials) != 2:
                raise ImproperlyConfigured('Invalid setting %s with value %r' % (key, value))
            MSP_CLIENT_CREDENTIALS.append(tuple(client_credentials))

    PORTAIL_CITOYEN_TEMPLATE_VARS['MSP'] = 1

try:
    from local_settings import *
except ImportError, e:
    if 'local_settings' in e.args[0]:
        pass

if RAVEN_CONFIG_DSN:
    RAVEN_CONFIG = {
            'dsn': RAVEN_CONFIG_DSN,
            }
    INSTALLED_APPS += ('raven.contrib.django.raven_compat', )
    LOGGING['handlers']['sentry'] = {
            'level': 'ERROR',
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
            }
    LOGGING['loggers']['raven'] = {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
            }
    LOGGING['loggers']['sentry.errors'] = {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
            }
    for logger in LOGGING['loggers'].values():
        logger['handlers'].append('sentry')

if USE_DEBUG_TOOLBAR:
    try:
        import debug_toolbar
        MIDDLEWARE_CLASSES += ('debug_toolbar.middleware.DebugToolbarMiddleware',)
        INSTALLED_APPS += ('debug_toolbar',)
    except ImportError:
        print "Debug toolbar missing, not loaded"

try:
    import cmsplugin_blurp
    INSTALLED_APPS += (
        'cmsplugin_blurp',
    )
except ImportError:
    pass

CMS_PLUGIN_BLURP_RENDERERS = {
    'tipi': {
        'name': 'Formulaire de paiement TIPI',
        'class': 'cmsplugin_blurp.renderers.template.TemplateRenderer',
        'refresh': 3600,
        'template': 'tipi',
        'template_name': 'blurps/tipi.html',
        'ajax': False,
    },
    'compte_lecteur': {
        'name': 'Compte lecteur',
        'class': 'cmsplugin_blurp.renderers.data_source.Renderer',
        'sources': [
            {
                'slug': 'compte_lecteur',
                'url': 'https://ermes-test-entrouvert.montpellier-agglo.com/mandaye/json?nameid={{federations.service_30.links.0}}',
                'parser_type': 'json',
                'verify_certificate': True,
                'allow_redirects': True
            }
        ],
        'refresh': 10,
        'template': u'{{ compte_lecteur }}',
        'template_name': 'blurps/compte_lecteur.html',
        'ajax': False,
    }
}

SERIALIZATION_MODULES = {
        'json': 'authentic2.serializers',
}
