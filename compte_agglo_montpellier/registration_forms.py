import re


from django.utils.translation import ugettext_lazy as _
from django import forms
from django.contrib.auth.forms import SetPasswordForm as OldSetPasswordForm, \
    PasswordChangeForm as OldPasswordChangeForm
from captcha.fields import CaptchaField

from authentic2.registration_backend.forms import RegistrationForm as AuthenticRegistrationForm

PASSWORD_RE = re.compile(r'^.{6,}$')
PASSWORD_HELP_TEXT = _('At least 6 characters')

class RegistrationForm(AuthenticRegistrationForm):
    # see http://dev.entrouvert.org/issues/3203
    captcha = CaptchaField(label=_('Give (with numbers) the result of the operation'))

    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)
        self.fields['password1'].help_text = PASSWORD_HELP_TEXT

    def clean_password1(self):
        global PASSWORD_RE

        password = self.cleaned_data['password1']
        if not PASSWORD_RE.match(password):
            raise forms.ValidationError(_('invalid password'))
        return password

class SetPasswordMixin(object):
    def __init__(self, *args, **kwargs):
        super(SetPasswordMixin, self).__init__(*args, **kwargs)
        self.fields['new_password1'].help_text = PASSWORD_HELP_TEXT

    def clean_new_password1(self):
        global PASSWORD_RE

        password = self.cleaned_data['new_password1']
        if not PASSWORD_RE.match(password):
            raise forms.ValidationError(_('invalid password'))
        return password

class SetPasswordForm(SetPasswordMixin, OldSetPasswordForm):
    pass

class PasswordChangeForm(SetPasswordMixin, OldPasswordChangeForm):
    pass
