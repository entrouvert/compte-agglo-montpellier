"""
This file was generated with the customdashboard management command, it
contains the two classes for the main dashboard and app index dashboard.
You can customize these classes as you want.

To activate your index dashboard add the following to your settings.py::
    ADMIN_TOOLS_INDEX_DASHBOARD = 'authentic2.dashboard.CustomIndexDashboard'

And to activate the app index dashboard::
    ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'authentic2.dashboard.CustomAppIndexDashboard'
"""

from django.utils.translation import ugettext_lazy as _

from admin_tools.dashboard import modules, Dashboard, AppIndexDashboard

_('wcs instance')
_('wcs instances')

class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for authentic2.
    """
    def init_with_context(self, context):
        # append an app list module for "Applications"
        self.children.append(modules.ModelList(
            _('Users and roles'),
            models=('portail_citoyen.models.Citoyen',
                'portail_citoyen.models.Role'),
        ))
        self.children.append(modules.ModelList(
            _('Services'),
            models=(
                'authentic2.saml.models.LibertyProvider',
                'authentic2.saml.models.LibertyFederation',
                'authentic2.saml.models.SPOptionsIdPPolicy',
                'authentic2.saml.models.IdPOptionsSPPolicy',
                'authentic2.attribute_aggregator.models.AttributeSource',
                'authentic2.idp.models.AttributePolicy',
                'authentic2.idp.models.AttributeList',
                'authentic2.idp.models.AttributeItem',
            ),
        ))
        self.children.append(modules.ModelList(
            _('Contents'),
            models=(
                'cms.models.pagemodel.Page',
                'portail_citoyen.apps.data_source_plugin.models.DataSource',
                'feed_plugin.models.Feed',
            ),
        ))

        self.children.append(modules.ModelList(
            _('Announces'),
            models=(
                'portail_citoyen_announces.*',
            ),
        ))
        self.children.append(modules.ModelList(
            _('Municipal sites'),
            models=(
                'wcsinst.*',
            ),
        ))

        # append a recent actions module
        self.children.append(modules.RecentActions(_('Recent Actions'), 5))

        # append another link list module for "support".
        self.children.append(modules.LinkList(
            _('Support'),
            children=[
                {
                    'title': _('Project site'),
                    'url': 'http://dev.entrouvert.org/projects/teleservices-agglo-montpellier/',
                    'external': True,
                },
                {
                    'title': _('Report a bug'),
                    'url': 'http://dev.entrouvert.org/projects/teleservices-agglo-montpellier/issues/new',
                    'external': True,
                },
            ]
        ))


class CustomAppIndexDashboard(AppIndexDashboard):
    """
    Custom app index dashboard for authentic2.
    """

    # we disable title because its redundant with the model list module
    title = ''

    def __init__(self, *args, **kwargs):
        AppIndexDashboard.__init__(self, *args, **kwargs)

        # append a model list module and a recent actions module
        self.children += [
            modules.ModelList(self.app_title, self.models),
            modules.RecentActions(
                _('Recent Actions'),
                include_list=self.get_app_content_types(),
                limit=5
            )
        ]

    def init_with_context(self, context):
        """
        Use this method if you need to access the request context.
        """
        return super(CustomAppIndexDashboard, self).init_with_context(context)
