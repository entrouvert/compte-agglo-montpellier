from django.contrib import admin

from portail_citoyen.admin import WcsInstancePortailCitoyenAdmin

from wcsinst.wcsinst.models import WcsInstance

class WcsInstanceCAMAdmin(WcsInstancePortailCitoyenAdmin):
    fieldsets = ((None, {'fields': ('title', 'domain', 'link'),}),)


admin.site.unregister(WcsInstance)
admin.site.register(WcsInstance, WcsInstanceCAMAdmin)
