import re
from django import forms
from django.utils.translation import ugettext_lazy as _


from authentic2.forms import UserProfileForm as Authentic2UserProfileForm


class UserProfileForm(Authentic2UserProfileForm):
    def __init__(self, *args, **kwargs):
        super(UserProfileForm, self).__init__(*args, **kwargs)
        self.fields['phone'].help_text = None
        self.fields['mobile'].help_text = None

    class Meta(Authentic2UserProfileForm.Meta):
        widgets = {
                'phone': forms.TextInput(attrs={'placeholder': _('such as 0499999999')}),
                'mobile': forms.TextInput(attrs={'placeholder': _('such as 0499999999')}),
        }
