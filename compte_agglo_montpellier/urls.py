from django.conf.urls import include, patterns, url
from django.shortcuts import redirect
from django.conf import settings


from portail_citoyen.urls import urlpatterns as pc_urls

import cms.cms_toolbar

def __django_cms_tollbar_request_hook_get(self):
    request = self.request
    if 'cms-toolbar-logout' in request.GET:
        return redirect('auth_logout')
cms.cms_toolbar.CMSToolbar._request_hook_get = __django_cms_tollbar_request_hook_get


urlpatterns = patterns('',
    url('^announces/', include('portail_citoyen_announces.urls')),
)

if 'cmsplugin_blurp' in settings.INSTALLED_APPS:
    urlpatterns += patterns('',
            url('^blurps/', include('cmsplugin_blurp.urls')),
    )

urlpatterns += patterns('',
            url(r'^captcha/', include('captcha.urls')),
            url(r'^cms-ajax-text-plugin/', include('cms_ajax_text_plugin.urls')),
)

urlpatterns += pc_urls
