Bonjour,

Vous avez demandé à ouvrir un compte citoyen sur le portail de Montpellier
Agglo ou sur le portail de votre commune, nous vous en remercions.

Votre identifiant d’accès est : {{ user.username }}

Vous pourrez utiliser ce même compte aussi bien pour vos démarches sur le
portail de Montpellier Agglo que sur le portail de démarches en ligne de votre
commune si celle-ci le propose.

Pour confirmer la création et activer votre compte, cliquez sur le lien
suivant :

http://{{ site.domain }}{% url 'registration_activate' activation_key %}

Ce lien est valable pendant {{ expiration_days }} jours.

Ce compte personnalisé et sécurisé est le vôtre! Il vous permet de :

- Accomplir vos démarches en ligne 24h/24 auprès de Montpellier Agglo et de
  votre commune dans un premier temps puis à terme auprès de la médiathèque,
  de l’écolothèque, du conservatoire.
- Suivre l’état de vos démarches en ligne
- Gérer vos inscriptions à nos newsletters et à nos flux d’infos
- Modifier vos informations personnelles
- Modifier votre mot de passe

N’hésitez pas à compléter toutes vos coordonnées. Ainsi, vous n’aurez plus
besoin de les réécrire dans les formulaires, ces informations seront
directement préremplies. Pour y accéder, rendez-vous sur le portail, rubrique
Mon compte.

Nous vous remercions d’utiliser notre portail de e-services.

Les équipes de Montpellier Agglo.

--------------------------------------------------
Important : Cet email est généré par un automate, merci de ne pas y répondre.
