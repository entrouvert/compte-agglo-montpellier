#! /usr/bin/env python

import glob
import re
import sys
import os

from setuptools import setup, find_packages
from setuptools.command.install_lib import install_lib as _install_lib
from distutils.command.build import build as _build
from distutils.command.sdist import sdist
from distutils.cmd import Command

class compile_translations(Command):
    description = 'compile message catalogs to MO files via django compilemessages'
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        try:
            from django.core.management.commands.compilemessages import \
                compile_messages
            for path in ['compte_agglo_montpellier']:
                if not os.path.exists(os.path.join(path, 'locale')):
                    continue
                curdir = os.getcwd()
                os.chdir(os.path.realpath(path))
                compile_messages(sys.stderr)
                os.chdir(curdir)
        except ImportError:
            print
            sys.stderr.write('!!! Please install Django >= 1.4 to build translations')
            print
            print

class build(_build):
    sub_commands = [('compile_translations', None)] + _build.sub_commands

class eo_sdist(sdist):

    def run(self):
        print "creating VERSION file"
        if os.path.exists('VERSION'):
            os.remove('VERSION')
        version = get_version()
        version_file = open('VERSION', 'w')
        version_file.write(version)
        version_file.close()
        sdist.run(self)
        print "removing VERSION file"
        if os.path.exists('VERSION'):
            os.remove('VERSION')

class install_lib(_install_lib):
    def run(self):
        self.run_command('compile_translations')
        _install_lib.run(self)

def get_version():

    version = None
    if os.path.exists('VERSION'):
        version_file = open('VERSION', 'r')
        version = version_file.read()
        version_file.close()
        return version
    for d in glob.glob('*'):
        if not os.path.isdir(d):
            continue
        module_file = os.path.join(d, '__init__.py')
        if not os.path.exists(module_file):
            continue
        for v in re.findall("""__version__ *= *['"](.*)['"]""",
                open(module_file).read()):
            assert version is None
            version = v
        if version:
            break
    assert version is not None
    if os.path.exists('.git'):
        import subprocess
        p = subprocess.Popen(['git','describe','--dirty','--match=v*'],
                stdout=subprocess.PIPE)
        result = p.communicate()[0]
        assert p.returncode == 0, 'git returned non-zero'
        new_version = result.split()[0][1:]
        assert not new_version.endswith('-dirty'), 'git workdir is not clean'
        assert new_version.split('-')[0] == version, '__version__ must match the last git annotated tag'
        version = new_version.replace('-', '.')
    return version


setup(name="compte-agglo-montpellier",
      version=get_version(),
      license="AGPLv3 or later",
      description="Compte agglo montpellier",
      author="Entr'ouvert",
      author_email="info@entrouvert.org",
      maintainer="Benjamin Dauvergne",
      maintainer_email="info@entrouvert.com",
      include_package_data=True,
      url='http://dev.entrouvert.org/projects/teleservices-agglo-montpellier',
      package_data={
          '': [
               'templates/**.html',
               'templates/**.txt',
               'static/**.png',
               'static/**.gif',
               'static/**.css',
               'static/**.js',
               'locale/**.mo',
               'fixtures/*.json',
              ]
      },
      packages=find_packages(),
      scripts=('compte-agglo-montpellier',),
      setup_requires=[
            'django>=1.5.1,<1.6',
      ],
      install_requires=[
            'portail-citoyen<1.0',
            'portail-citoyen-announces>=0.1,<0.2',
            'cmsplugin-embedded-menu==0.0.7',
            'gunicorn',
            'demjson',
            'django-simple-captcha==0.4.0',
            'raven',
            'wcsinst<1.0',
            'south>=0.8.4',
            'django-cms>=3.0,<=3.0.3',
            'djangocms-text-ckeditor',
            'django-cms-ajax-text-plugin>=2.1.0'
            'djangocms-video',
      ],
      dependency_links = [
            'git+git://repos.entrouvert.org/portail-citoyen#egg=portail-citoyen-0.1.99999',
            'git+git://repos.entrouvert.org/portail-citoyen-announces#egg=portail-citoyen-announces-0.1.99999',
            'git+git://repos.entrouvert.org/wcsinst#egg=wcsinst-0.9999999',
      ],
      cmdclass={'build': build, 'install_lib': install_lib,
          'compile_translations': compile_translations,
          'sdist': eo_sdist},
)
