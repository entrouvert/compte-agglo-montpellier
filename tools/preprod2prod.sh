#!/bin/sh

DEST_DIR="/var/tmp/montpellier/fixtures"

if [ ! -d $DEST_DIR ]; then
    mkdir -p $DEST_DIR
fi

echo "Building file pages-preprod.json and pages-prod.json in fixtures..."
compte-agglo-montpellier dumpdata sites cms cmsplugin_text_wrapper file \
    googlemap link picture snippet teaser video login_plugin data_source_plugin \
    a2_service_list_plugin feed_plugin > $DEST_DIR/pages-preprod.json
echo "Changing URLs for prod"
for preprod in $DEST_DIR/*-preprod.json; do
    sed -e 's#http://www-test.entrouvert.montpellier-agglo.com#http://www.montpellier-agglo.com#g' \
        -e 's#https://idp-test-entrouvert.montpellier-agglo.com#https://compte-citoyen.montpellier-agglo.com#g' \
        -e 's#"idp-test-entrouvert.montpellier-agglo.com"#"compte-citoyen.montpellier-agglo.com"#g' \
        -e 's#orig=idp-test-entrouvert.montpellier-agglo.com#orig=compte-citoyen.montpellier-agglo.com#g' \
        -e 's#https://eservices-test-entrouvert.montpellier-agglo.com#https://eservices.montpellier-agglo.com#g' \
        -e 's#passerelle-test-entrouvert.montpellier-agglo.com#passerelle.montpellier-agglo.com#g' \
        $preprod >`echo $preprod | sed s,preprod,prod,`; echo " - processed " $preprod
done


