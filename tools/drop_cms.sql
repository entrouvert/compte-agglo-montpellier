BEGIN;
 DROP TABLE auquotidien_plugin_auquotidienapi CASCADE;
 DROP TABLE cms_cmsplugin CASCADE;
 DROP TABLE cms_globalpagepermission CASCADE;
 DROP TABLE cms_globalpagepermission_sites CASCADE;
 DROP TABLE cms_page CASCADE;
 DROP TABLE cms_page_placeholders CASCADE;
 DROP TABLE cms_pagemoderatorstate CASCADE;
 DROP TABLE cms_pagepermission CASCADE;
 DROP TABLE cms_pageuser CASCADE;
 DROP TABLE cms_pageusergroup CASCADE;
 DROP TABLE cms_placeholder CASCADE;
 DROP TABLE cms_title CASCADE;
 DROP TABLE cmsplugin_a2servicelistplugin CASCADE;
 DROP TABLE cmsplugin_announcelistplugin CASCADE;
 DROP TABLE cmsplugin_announcesubscribeplugin CASCADE;
 DROP TABLE cmsplugin_datasourceplugin CASCADE;
 DROP TABLE cmsplugin_file CASCADE;
 DROP TABLE cmsplugin_googlemap CASCADE;
 DROP TABLE cmsplugin_link CASCADE;
 DROP TABLE cmsplugin_loginplugin CASCADE;
 DROP TABLE cmsplugin_passerelleregisterplugin CASCADE;
 DROP TABLE cmsplugin_picture CASCADE;
 DROP TABLE cmsplugin_rawinlinetemplateplugin CASCADE;
 DROP TABLE cmsplugin_selectuserfeed CASCADE;
 DROP TABLE cmsplugin_showuserfeed CASCADE;
 DROP TABLE cmsplugin_snippetptr CASCADE;
 DROP TABLE cmsplugin_teaser CASCADE;
 DROP TABLE cmsplugin_text CASCADE;
 DROP TABLE cmsplugin_video CASCADE;
 DROP TABLE data_source_plugin_datasource CASCADE;
 DROP TABLE data_source_plugin_plugindatasource CASCADE;
 DROP TABLE feed_plugin_feed CASCADE;
 DROP TABLE feed_plugin_feedpreference CASCADE;
 DROP TABLE menus_cachekey CASCADE;
 DROP TABLE snippet_snippet CASCADE;
COMMIT;
