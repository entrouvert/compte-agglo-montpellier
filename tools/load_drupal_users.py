#!/usr/bin/env python
import os
import sys
import csv


sys.path.append('portail-citoyen.git')
sys.path.append('authentic2.git')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "portail_citoyen.settings")

from portail_citoyen.models import Citoyen
from authentic2.saml.models import LibertyFederation
from authentic2.hashers import Drupal7PasswordHasher
from django.db import transaction

#-[ RECORD 1 ]-------------+--------------------------------------------------------------------------------------------------------
#id                        | 23
#user_id                   | 3
#idp_id                    | http://idp-montpellier.entrouvert.org/idp/saml2/metadata
#sp_id                     | http://preview-eservices.montpellier-agglo.com/simplesamlphp/module.php/saml/sp/metadata.php/default-sp
#name_id_qualifier         | http://idp-montpellier.entrouvert.org/idp/saml2/metadata
#name_id_format            | urn:oasis:names:tc:SAML:2.0:nameid-format:persistent
#name_id_content           | _9903A47299512211F49F9E7931183761
#name_id_sp_name_qualifier | http://preview-eservices.montpellier-agglo.com/simplesamlphp/module.php/saml/sp/metadata.php/default-sp
#name_id_sp_provided_id    | 

idp_id = 'http://idp-montpellier.entrouvert.org/idp/saml2/metadata'
sp_id = 'http://preview-eservices.montpellier-agglo.com/simplesamlphp/module.php/saml/sp/metadata.php/default-sp'
hasher = Drupal7PasswordHasher()

rows = csv.DictReader(file('/tmp/django_users2.csv'), fieldnames=['uid', 'username',
    'email', 'firstname', 'name_id'])
rows2 = csv.DictReader(file('/tmp/drupal_users.csv'), fieldnames=['uid', 'username',
    'password', 'firstname'])
rows.next()
rows2.next()
with transaction.commit_on_success():
    i = 1
    for row, row2 in zip(rows, rows2):
        print '%04d' % i, '\r',
        assert row['uid'] == row2['uid']
        user = Citoyen.objects.create(username=row['username'],
            email=row['email'],
            first_name=row['firstname'].decode('utf8'),
            nickname='drupal',
            password=hasher.from_drupal(row2['password']))
        LibertyFederation.objects.create(
            user=user, idp_id=idp_id, sp_id=sp_id, name_id_qualifier=idp_id,
            name_id_format='urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',
            name_id_content=row['name_id'], name_id_sp_name_qualifier=sp_id)
        i += 1
    print
