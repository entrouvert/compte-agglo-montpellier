SCRIPT=./compte-agglo-montpellier
BASE=./compte_agglo_montpellier

all:
	true

show-domains-occurences:
	grep -Hno 'http.\?://[a-zA-Z0-9.-]*' compte_agglo_montpellier/fixtures/*.json

show-domains:
	grep -ho 'http.\?://[a-zA-Z0-9.-]*' compte_agglo_montpellier/fixtures/*.json | sort -u

update-pages-and-saml:
	@echo Building file saml-preprod.json, pages-preprod.json, salm-dev.json and pages-dev.json in fixtures...
	@./run.sh dumpdata -e attribute_aggregator.userattributeprofile -e \
		saml.keyvalue -e authentic2.deleteduser -e saml.libertyassertion -e saml.libertyfederation -e \
		saml.libertysession -e saml.libertysessiondump idp saml authentic2 \
		attribute_aggregator saml | jsonlint -f > $(BASE)/fixtures/saml-preprod.json
	@./run.sh dumpdata sites cms cmsplugin_text_wrapper file \
		googlemap link picture snippet teaser video login_plugin data_source_plugin \
		a2_service_list_plugin feed_plugin.selectuserfeed feed_plugin.showuserfeed | jsonlint -f > \
		$(BASE)/fixtures/pages-preprod.json
	@echo Changing URLs for dev
	@for preprod in $(BASE)/fixtures/*-preprod.json; do sed -e 's#http://www-test.entrouvert.montpellier-agglo.com#http://drupal.montpellier.entrouvert.org#g' \
		   -e 's#https://idp-test-entrouvert.montpellier-agglo.com#http://idp-montpellier.entrouvert.org#g' \
			 -e 's#"idp-test-entrouvert.montpellier-agglo.com"#"idp-montpellier.entrouvert.org"#g' \
			 -e 's#orig=idp-test-entrouvert.montpellier-agglo.com#orig=idp-montpellier.entrouvert.org#g' \
			 -e 's#https://eservices-test-entrouvert.montpellier-agglo.com#http://eservices-montpellier.entrouvert.org#g' \
			 $$preprod >`echo $$preprod | sed s,preprod,dev,`; echo " - processed " $$preprod; done
	@git commit -v $(BASE)/fixtures/

update-feeds:
	@echo Building file feeds.json in fixtures...
	@./run.sh dumpdata feed_plugin.feed | jsonlint -f > \
		$(BASE)/fixtures/feeds.json
	@git commit -v $(BASE)/fixtures/feeds.json

update-groups:
	@echo Building file groups.json in fixtures...
	@./run.sh dumpdata -n auth.group | jsonlint -f >$(BASE)/fixtures/groups.json
	@git commit -v $(BASE)/fixtures/groups.json

update-users:
	@echo Building file users.json in fixtures...
	@./run.sh dumpdata portail_citoyen.citoyen | jsonlint -f >$(BASE)/fixtures/users.json
	@git commit -v $(BASE)/fixtures/users.json

update-dev:
	ssh root@montpellier-dev /var/vhosts/idp-montpellier.entrouvert.org/virtualenv/bin/pip install -U git+git://repos.entrouvert.org/compte-agglo-montpellier.git/
