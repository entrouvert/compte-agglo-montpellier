BASE=`dirname $0`

ENV=${ENV:-dev}

$BASE/run.sh loaddata --traceback groups feeds categories users-${ENV} saml-${ENV} pages-${ENV}
