PROJECT=compte-agglo-montpellier
CTL=${PROJECT}
VENV=${PROJECT}-venv

if [ "$VIRTUAL_ENV" = "" ]; then
	if which mkvirtualenv >/dev/null 2>&1; then
		workon $PROJECT || (mkvirtualenv $PROJECT; workon $PROJECT)
	else 
		if [ ! -d $VENV ]; then
			virtualenv --system-site-packages $VENV 2>/dev/null || virtualenv $VENV
		fi
		. ./$VENV/bin/activate
	fi
fi
pip install --upgrade setuptools
pip install --upgrade pip
pip install --upgrade pyOpenSSL==0.13 ndg-httpsclient requests pyasn1
sed -i 's/^MAX = 64/MAX = 200/' $VENV/lib/python*/site-packages/ndg/httpsclient/subj_alt_name.py
pip install --upgrade --process-dependency-links -r requirements.txt
if [ ! -f $PROJECT.db ]; then
	./$CTL syncdb --all --noinput
	./$CTL migrate --fake
	./load-base-data.sh
fi
if [ "$1" != "norun" ]; then
	./$CTL runserver
fi
